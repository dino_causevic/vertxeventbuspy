from eventBusPy.lib.Eventbus import DeliveryOption
from eventBusPy.lib.Eventbus import Eventbus
import json


# replyHanlder (self,error,message)
# handlers (self,message)

class Client:

    # replyHandler
    def replyhandler(self, error, message):
        if error != None:
            print(error)
        if message != None:
            print(message['body'])

    # Handler for errors and msg
    def Handler(self, message):
        if message != None:
            print(message['body'])


eb = Eventbus.Eventbus(host='127.0.0.1', port=7000)

# jsonObject -body
import time
body = {'time': time.time(), "from": "ping"}

# DeliveryOption
do = DeliveryOption.DeliveryOption()
do.setTimeInterval(1)

# send
# eb.send('ping', body, do)

eb.registerHandler('ping', Client.Handler)

# close after 5 seconds
# eb.closeConnection(10)
# time.sleep(10)
