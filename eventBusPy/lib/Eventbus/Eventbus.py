import socket
import json
import struct
import time
from time import sleep
import threading
import logging

logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)

stdout_logger = logging.StreamHandler()
logger.addHandler(stdout_logger)


class Eventbus:
    # sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # constructor
    def __init__(self, host='localhost', port=7000, TimeOut=0.1, TimeInterval=10.0):
        self.host = host
        self.port = port
        self.reconnectTimer = 2.0
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.handlers = {}
        self.state = 0
        self.replyHandler = {}
        self.writable = True

        if TimeOut < 0.01:
            self.TimeOut = 0.01
        else:
            self.TimeOut = TimeOut

        self.TimeInterval = TimeInterval

        self.check_recv_trigger_timer = 5.0
        self.closing_event = threading.Event()
        self.reconnecting_phase = threading.Event()
        self.reconnecting_phase.clear()

        self.closing_event.clear()

        self.check_recv_thread = threading.Thread(target=self.check_reconnection_trigger)
        self.check_recv_thread.start()

        # connect
        try:
            self.state = 1
            self.sock.settimeout(self.TimeOut)
            self.sock.connect((self.host, self.port))
            logger.debug(f"Local port {self.sock.getsockname()}")

            self.t1 = threading.Thread(target=self.receivingThread)
            self.t1.start()
            self.state = 2
            logger.debug(f"Connected on {self.host}:{self.port}")
        except IOError as e:
            logger.error(f"Issues with connecting on bus. {e}")
            logger.exception(e)
            self._reconnect()
        except Exception as e:
            logger.error(f"Undefined Error: {e}")

    def check_reconnection_trigger(self):

        while not self.closing_event.is_set():

            if self.reconnecting_phase.set():
                logger.debug("Reconnecting phase is set, skip the check while reconnecting phase is not done.")
                sleep(2*self.check_recv_trigger_timer)
                continue

            if not getattr(self, "t1", None) or not self.t1:
                logger.debug("Receive thread is not created, will not check further until thread is started.")
                sleep(1.5*self.check_recv_trigger_timer)
                continue

            logger.debug("Checking reconnect trigger ...")

            if not self.t1.is_alive():
                logger.debug("Receive thread is not alive, initializing reconnection ...")
                self._reconnect()
            else:
                logger.debug(f"Receive thread is alive, going to sleep on {self.check_recv_trigger_timer} seconds.")

            sleep(self.check_recv_trigger_timer)

        logger.debug("Closing reconnect trigger thread ...")

    def _reconnect(self):

        while True:
            try:
                self.reconnecting_phase.set()

                if getattr(self, "sock") and self.sock:
                    self.closeConnection(1)

                logger.debug(f"Reconnecting on {self.host}:{self.port}, sleep time {self.reconnectTimer} ...")
                self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.sock.settimeout(self.TimeOut)
                self.sock.connect((self.host, self.port))
                logger.debug(f"Local port {self.sock.getsockname()}")
                self.state = 2
                logger.debug("Connected.")

                logger.debug("Initializing receive thread ...")
                self.reRegisterHandlers()
                self.t1 = threading.Thread(target=self.receivingThread)
                self.t1.start()

                break
            except Exception:
                sleep(self.reconnectTimer)
                continue

        self.reconnecting_phase.clear()

        return True


    # Connection send and receive--------------------------------------------------------------------

    def isConnected(self):
        if self.state is 2:
            return True
        else:
            return False

    def sendFrame(self, message_s):

        try:
            message = message_s.encode('utf-8')
            frame = struct.pack('!I', len(message)) + message
            self.sock.sendall(frame)
        except Exception as ex:
            logger.error(f"Problem with sending {frame}.")
            logger.exception(ex)

    def receive(self):

        try:
            if self.state < 3:  # closing socket
                len_str = self.sock.recv(4)
            else:
                return False

            try:
                len1 = struct.unpack("!i", len_str)[0]
            except Exception as ex:
                logging.error(f"Problem with unpacking {len_str}.")
                return False

            if self.state < 3:  # closing socket
                payload = self.sock.recv(len1)
            else:
                return False

            json_message = payload.decode('utf-8')
            message = json.loads(json_message)

            if message.get('type') == 'message':
                # failure message
                if 'address' not in message.keys():
                    logger.warning(f"Message failure, address doesn't exists in {message}.")
                else:
                    try:
                        # handlers
                        if 'address' in message and self.handlers.get(message['address'], None) is not None:
                            for handler in self.handlers[message['address']]:
                                handler(message)
                            self.replyHandler = None

                    except KeyError:
                        # replyHandler
                        try:
                            if self.replyHandler['address'] == message['address']:
                                self.replyHandler['replyHandler'](self, None, message)
                                self.replyHandler = None
                        except KeyError:
                            logger.warning(f"No handler for {message.get('address')}.")

            elif message.get('type') == 'err':
                try:
                    self.replyHandler['replyHandler'](self, message, None)
                    self.replyHandler = None
                except:
                    logger.warning(f"Error {message}.")
            else:
                logger.warning(f"Unknown type. Type: {message.get('type')}.")

            return True
        except socket.timeout:
            return True
        except Exception as e:
            logger.warning(f"Undefined Error. {e}")
            return False

    # send error message

    def receivingThread(self):

        while self.state < 3:
            if self.writable is False and self.receive() is False:
                logger.info("Connection is closed. Reconnecting phase will be initialize.")
                break

    def closeConnection(self, timeInterval=30, close_app=False):

        logger.debug(f"Closing connection in {timeInterval}.")

        if self.state == 1:
            self.sock.close()
            return

        self.state = 3
        time.sleep(timeInterval)
        try:
            self.sock.close()
        except Exception as e:
            logger.warning(f"Undefined Error. {e}")

        self.state = 4

        if close_app:
            self.closing_event.set()

    # address-string
    # body - json object
    # deliveryOption - object
    # replyHandler -function

    def send(self, address, body=None, deliveryOption=None, replyHandler=None):
        if self.isConnected() is True:
            message = None

            if callable(deliveryOption) is True:
                replyHandler = deliveryOption
                deliveryOption = None

            if deliveryOption is not None:
                headers = deliveryOption.headers
                replyAddress = deliveryOption.replyAddress
                timeInterval = deliveryOption.timeInterval
            else:
                headers = None
                replyAddress = None
                timeInterval = self.TimeInterval

            message = json.dumps(
                {'type': 'send', 'address': address, 'replyAddress': replyAddress, 'headers': headers, 'body': body, }
            )

            self.writable = True
            self.sendFrame(message)

            # replyHandler
            if replyAddress is not None and replyHandler is not None:
                self.replyHandler = {
                    'address': replyAddress,
                    'replyHandler': replyHandler
                }

            self.writable = False
            i = 0.0
            while timeInterval / self.TimeOut >= i:
                time.sleep(self.TimeOut)
                if self.replyHandler is None:
                    break

                if (timeInterval / self.TimeOut) == i and self.replyHandler is not None:

                    try:
                        handler = self.replyHandler.get('replyHandler', None)
                        if handler:
                            handler(self, 'Time Out Error', None)
                            self.replyHandler = None
                    finally:
                        break

                i += 1.0
        else:
            logger.warning(f"Invalid state error {self.state}.")

    # address-string
    # body - json object
    # deliveryOption -object
    def publish(self, address, body, deliveryOption=None):

        if self.isConnected() is True:
            if deliveryOption is not None:
                headers = deliveryOption.headers
                replyAddress = deliveryOption.replyAddress
            else:
                headers = None
                replyAddress = None

            if replyAddress is None:
                message = json.dumps({'type': 'send', 'address': address, 'headers': headers, 'body': body, })
            else:
                message = json.dumps({'type': 'send', 'address': address, 'headers': headers, 'body': body, })

            self.writable = True
            self.sendFrame(message)
            self.writable = False

        else:
            logger.warning(f"Invalid state error. {self.state}")

    def reRegisterHandlers(self):

        logger.debug("Re-initialized register of handlers ...")
        if self.isConnected():

            for h in self.handlers:
                try:
                    logger.debug(f"Processing {h} handlers ...")
                    message = json.dumps({'type': 'register', 'address': h})
                    self.writable = True
                    self.sendFrame(message)
                    self.writable = False
                    time.sleep(self.TimeOut)
                except Exception as ex:
                    logger.error(f"Problem with registering handler {h}.")
                    logger.error(ex)
        else:
            logger.error(f"Invalid connection state {self.state}.")


    # address-string
    # deliveryOption -object
    # replyHandler -function
    def registerHandler(self, address, handler):

        if self.isConnected() is True:
            message = None

            if callable(handler) is True:
                try:
                    if (address not in self.handlers.keys()) or (self.handlers[address] is None):
                        self.handlers[address] = []
                        message = json.dumps({'type': 'register', 'address': address, })
                        self.writable = True
                        self.sendFrame(message)
                        self.writable = False
                        time.sleep(self.TimeOut)
                except KeyError:
                    self.handlers[address] = []

                try:
                    self.handlers[address].append(handler)
                except Exception as e:
                    logger.warning(f"Registration failed. {e}")
            else:
                logger.warning("Registration failed. Function is not callable.")
        else:
            logger.warning(f"Invalid state error {self.state}.")

    # address-string
    # deliveryOption -object
    # replyHandler -function
    def unregisterHandler(self, address):
        if self.isConnected() is True:
            message = None
            try:
                if self.handlers[address] is not None:
                    if len(self.handlers) == 1:
                        message = json.dumps({'type': 'unregister', 'address': address, })
                        self.sendFrame(message)
                    del self.handlers[address]
            except:
                logger.warning(f"Unknown address: {address}")

        else:
            logger.error(f"Error occurs, invalid state error: {self.state}.")

