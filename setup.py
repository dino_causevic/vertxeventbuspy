from setuptools import setup, find_packages

setup(name='eventBusPy',
      version='0.1',
      description='Event Bus for vertx.',
      author='Dino Causevic',
      author_email='dincaus@gmail.com',
      url='https://app.deveo.com/datazup/projects/etlpipes/repositories/VertxEventBusPy/tree/master',
      packages=find_packages())
